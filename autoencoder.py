import tensorflow as tf
import tensorflow.keras as keras
import sklearn
from leadec.data.IU.FFT.preprocessing_iu_fft import *
import matplotlib.pyplot as plt
import pandas as pd


def create_sequences(values, time_steps=1):
    output = []
    for i in range(len(values) - time_steps):
        output.append(values[i : (i + time_steps)])
    return np.stack(output)

n_components = 10
latent_dim = 2
epoch = 200


def load_x(path):
    patients_df = pd.read_json(path)
    x_read = np.array(patients_df['FFT_amplitude_array'].to_list(), dtype='object')

    # check if frequency grid is equal for all datapoints
    num_amps = list(map(lambda data: len(data), x_read))
    max_num_amps = np.max(num_amps)
    valid = num_amps == max_num_amps
    x = x_read[valid].tolist()
    return x

# fft_y_125_1 (630, 10)
#x_train_1 = preprocessing_iu_fft('data/IU/FFT/1_FFT_Y_125_1_ok.csv.json', n_components, -1, 1, False)
# fft_y_125_2 (627, 10)
#x_train_2 = preprocessing_iu_fft('data/IU/FFT/1_FFT_Y_125_2_ok.csv.json', n_components, -1, 1, False)

#x_test_1 = preprocessing_iu_fft('data/IU/FFT/1_FFT_Y_125_1_Nok.csv.json', n_components, -1, 1, False)
#x_test_2 = preprocessing_iu_fft('data/IU/FFT/1_FFT_Y_125_2_test.csv.json', n_components, -1, 1, False)

x_train_1 = load_x('data/IU/FFT/1_FFT_Y_125_1_ok.csv.json')
x_test_1 = load_x('data/IU/FFT/1_FFT_Y_125_1_Nok.csv.json')

pca1 = sklearn.decomposition.PCA(n_components=n_components)
pca2 = sklearn.decomposition.PCA(n_components=n_components)

x_train_1 = pca1.fit_transform(x_train_1)
x_test_1 = pca2.fit_transform(x_test_1)

minmax1 = sklearn.preprocessing.MinMaxScaler(feature_range=(0, 1))
minmax2 = sklearn.preprocessing.MinMaxScaler(feature_range=(0, 1))

x_train_1 = minmax1.fit_transform(x_train_1)
x_test_1 = minmax2.fit_transform(x_test_1)

x_train_1 = create_sequences(x_train_1)
x_train_1 = x_train_1[:, 0, :]
print("Training input shape: ", x_train_1.shape)

model = keras.Sequential()
model.add(keras.layers.Dense(n_components))
model.add(keras.layers.Dense(n_components//latent_dim, activation='relu'))
model.add(keras.layers.Dense(n_components, activation='relu'))

model.compile(optimizer="adam", loss='mse')
model.build((627, n_components))
model.summary()
history = model.fit(x_train_1, x_train_1, batch_size=128, validation_split=0.2, epochs=epoch)

plt.plot(history.history["loss"], label="Training Loss")
plt.plot(history.history["val_loss"], label="Validation Loss")
plt.legend()
plt.show()

x_train_pred = model.predict(x_train_1)
train_mae_loss = np.mean(np.abs(x_train_pred - x_train_1), axis=1)

plt.hist(train_mae_loss, bins=50)
plt.xlabel("Train MAE loss")
plt.ylabel("No of samples")
plt.show()

plt.plot(train_mae_loss)
plt.ylabel("Train MAE loss")
plt.xlabel("Datapoint")
plt.show()



# Get reconstruction loss threshold.
threshold = np.max(train_mae_loss)
print("Reconstruction error threshold: ", threshold)

# Checking how the first sequence is learnt
plt.plot(pca1.inverse_transform(x_train_1[0]))
plt.plot(pca1.inverse_transform(x_train_pred[0]))
plt.show()

# Create sequences from test values.
x_test = create_sequences(x_test_1)
x_test = x_test[:, 0, :]
print("Test input shape: ", x_test.shape)

# Get test MAE loss.
x_test_pred = model.predict(x_test)
test_mae_loss = np.mean(np.abs(x_test_pred - x_test), axis=1)
test_mae_loss = test_mae_loss.reshape((-1))

plt.hist(test_mae_loss, bins=50)
plt.xlabel("Test MAE loss")
plt.ylabel("No of samples")
plt.show()

# Checking how the first sequence is learnt
plt.plot(pca2.inverse_transform(x_test_1[300]))
plt.plot(pca2.inverse_transform(x_test_pred[300]))
plt.show()

plt.plot(test_mae_loss)
plt.ylabel("Test MAE loss")
plt.xlabel("Data Point")
plt.show()


# Detect all the samples which are anomalies.
anomalies = test_mae_loss > threshold
print("Number of anomaly samples: ", np.sum(anomalies))
print("Indices of anomaly samples: ", np.where(anomalies))

test=1