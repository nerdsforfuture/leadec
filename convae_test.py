import tensorflow as tf
import tensorflow.keras as keras
import sklearn
from leadec.data.IU.FFT.preprocessing_iu_fft import *
import matplotlib.pyplot as plt
import pandas as pd
import keras.layers as layers
import sklearn
import numpy as np

timestamp = pd.read_csv("data/IU/Time-series/1_rms_125_1_ok.csv")["timestamp"]
max_vel_x = pd.read_csv("data/IU/Time-series/1_rms_125_1_ok.csv")["max_vel_x"]
max_vel_y = pd.read_csv("data/IU/Time-series/1_rms_125_1_ok.csv")["max_vel_y"]
max_vel_z = pd.read_csv("data/IU/Time-series/1_rms_125_1_ok.csv")["max_vel_z"]
max_audio = pd.read_csv("data/IU/Time-series/1_rms_125_1_ok.csv")["max_audio"]

test_vel_x = pd.read_csv("data/IU/Time-series/1_rms_125_2_test.csv")["max_vel_x"]
test_vel_y = pd.read_csv("data/IU/Time-series/1_rms_125_2_test.csv")["max_vel_y"]
test_vel_z = pd.read_csv("data/IU/Time-series/1_rms_125_2_test.csv")["max_vel_z"]
test_audio = pd.read_csv("data/IU/Time-series/1_rms_125_2_test.csv")["max_audio"]

test_vel_x = pd.read_csv("data/IU/Time-series/1_rms_125_1_Nok.csv")["max_vel_x"]
test_vel_y = pd.read_csv("data/IU/Time-series/1_rms_125_1_Nok.csv")["max_vel_y"]
test_vel_z = pd.read_csv("data/IU/Time-series/1_rms_125_1_Nok.csv")["max_vel_z"]
test_audio = pd.read_csv("data/IU/Time-series/1_rms_125_1_Nok.csv")["max_audio"]

tt = np.array([max_vel_x, max_vel_y,  max_vel_z, max_audio]).T
#tt = np.array([max_vel_x]).T
df = pd.DataFrame(tt, columns=['x', 'y', 'z', 'db'])
#df = pd.DataFrame(tt, columns=['x'])

tt_test = np.array([test_vel_x, test_vel_y,  test_vel_z, test_audio]).T
#tt_test = np.array([test_vel_x]).T

df_test = pd.DataFrame(tt_test, columns=['x', 'y', 'z', 'db'])
#df_test = pd.DataFrame(tt_test, columns=['x'])

standard_scaler = sklearn.preprocessing.StandardScaler()
minmax_scaler = sklearn.preprocessing.MinMaxScaler()
df = standard_scaler.fit_transform(df)
#df = minmax_scaler.fit_transform(df)

df_test = standard_scaler.fit_transform(df_test)
#df_test = minmax_scaler.fit_transform(df_test)

TIME_STEPS = 180


# Generated training sequences for use in the model.
def create_sequences(values, time_steps=TIME_STEPS):
    output = []
    for i in range(len(values) - time_steps):
        output.append(values[i: (i + time_steps)])
    return np.stack(output)

x_train = create_sequences(df)
x_train2, x_val = sklearn.model_selection.train_test_split(x_train, test_size=0.2)
print("Training input shape: ", x_train.shape)


model = keras.Sequential()
model.add(layers.Input(shape=(TIME_STEPS, 1)))
model.add(layers.Conv1D(filters=32, kernel_size=7, padding="same", strides=2, activation="relu"))
model.add(layers.Dropout(rate=0.2))
model.add(layers.Conv1D(filters=16, kernel_size=7, padding="same", strides=2, activation="relu"))
model.add(layers.Conv1DTranspose(filters=16, kernel_size=7, padding="same", strides=2, activation="relu"))
model.add(layers.Dropout(rate=0.2))
model.add(layers.Conv1DTranspose(filters=32, kernel_size=7, padding="same", strides=2, activation="relu"))
model.add(layers.Conv1DTranspose(filters=1, kernel_size=7, padding="same"))

model.compile(optimizer="adam", loss="mse")
model.summary()

history = model.fit(x_train, x_train, epochs=40, batch_size=512, validation_split=0.2)

plt.plot(history.history["loss"], label="Training Loss")
plt.plot(history.history["val_loss"], label="Validation Loss")
plt.legend()
plt.show()

# Get train MAE loss.
x_train_pred = model.predict(x_train2)
x_val_pred = model.predict(x_val)
train_mae_loss = np.mean(np.abs(x_train_pred - x_train2), axis=1)

plt.hist(train_mae_loss, bins=50)
plt.xlabel("Train MAE loss")
plt.ylabel("No of samples")
plt.show()

# Get reconstruction loss threshold.
threshold = np.max(train_mae_loss)
print("Reconstruction error threshold: ", threshold)

# Checking how the first sequence is learnt
plt.plot(x_train2[0])
plt.plot(x_train_pred[0])
plt.title("Train Dataset")
plt.show()

plt.plot(x_val[0])
plt.plot(x_val_pred[0])
plt.title("Validation Dataset")
plt.show()

# Create sequences from test values.
x_test = create_sequences(df_test)
print("Test input shape: ", x_test.shape)

# Get test MAE loss.
x_test_pred = model.predict(x_test)
test_mae_loss = np.mean(np.abs(x_test_pred - x_test), axis=1)
test_mae_loss = test_mae_loss.reshape((-1))


x_test_pred_shaped = np.reshape(x_test_pred[:, :, 0], newshape=(180*105620,))
x_test_shaped = np.reshape(x_test[:, :, 0], newshape=(180*105620))

plt.plot(x_test_shaped[0:3000])
plt.plot(x_test_pred_shaped[0:3000])
plt.show()

plt.hist(test_mae_loss, bins=50)
plt.xlabel("test MAE loss")
plt.ylabel("No of samples")
plt.show()

# Detect all the samples which are anomalies.
anomalies = test_mae_loss > threshold
print("Number of anomaly samples: ", np.sum(anomalies))
print("Indices of anomaly samples: ", np.where(anomalies))

plt.plot(test_mae_loss)
plt.vlines(2447,np.min(test_mae_loss), np.max(test_mae_loss))
plt.xlabel("test MAE loss")
plt.ylabel("No of samples")
plt.show()