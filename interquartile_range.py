from os import pipe
from numpy.core.fromnumeric import transpose
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import StandardScaler
from sklearn.decomposition import PCA
from sklearn.pipeline import make_pipeline
import numpy as np

df = pd.read_csv('data/IU/Time-series/1_rms_125_1_ok.csv')
print(df)

df2 = df.drop(['timestamp', 'monitor_id', 'machinename'], axis=1)
names = df2.columns
x = df[names]

# scaler = StandardScaler()
# pca = PCA()
# pipeline=make_pipeline(scaler, pca)
# pipeline.fit(x)
# features = range(pca.n_components_)
# _ = plt.figure(figsize=(15, 5))
# _ = plt.bar(features, pca.explained_variance_)
# _ = plt.xlabel('PCA feature')
# _ = plt.ylabel('Variance')
# _ = plt.xticks(features)
# _ = plt.title("Importance of the Principal Components based on inertia")
# plt.show()

pca = PCA(n_components=2)
principal_components = pca.fit_transform(x)
principal_df = pd.DataFrame(data = principal_components, columns = ['pc1', 'pc2'])

# Calculate IQR for the 1st principal component (pc1)
q1_pc1, q3_pc1 = principal_df['pc1'].quantile([0.25, 0.75])
iqr_pc1 = q3_pc1 - q1_pc1
# Calculate upper and lower bounds for outlier for pc1
lower_pc1 = q1_pc1 - (1.5*iqr_pc1)
upper_pc1 = q3_pc1 + (1.5*iqr_pc1)
# Filter out the outliers from the pc1
df['anomaly_pc1'] = ((principal_df['pc1']>upper_pc1) | (principal_df['pc1']<lower_pc1)).astype('int')
# Calculate IQR for the 2nd principal component (pc2)
q1_pc2, q3_pc2 = principal_df['pc2'].quantile([0.25, 0.75])
iqr_pc2 = q3_pc2 - q1_pc2
# Calculate upper and lower bounds for outlier for pc2
lower_pc2 = q1_pc2 - (1.5*iqr_pc2)
upper_pc2 = q3_pc2 + (1.5*iqr_pc2)
# Filter out the outliers from the pc2
df['anomaly_pc2'] = ((principal_df['pc2']>upper_pc2) | (principal_df['pc2']<lower_pc2)).astype('int')
# Let's plot the outliers
a = df[np.logical_or(df['anomaly_pc1'] == 1, df['anomaly_pc2'] == 1)] #anomaly
_ = plt.figure(figsize=(18,6))
_ = plt.plot(df['max_audio'], color='blue', label='Normal')
_ = plt.plot(a['max_audio'], linestyle='none', marker='X', color='red', markersize=12, label='Anomaly')
# _ = plt.xlabel('Date and Time')
_ = plt.ylabel('Max Audio [db]')
_ = plt.legend(loc='best')
plt.show()


