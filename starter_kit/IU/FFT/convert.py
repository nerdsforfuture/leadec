import csv
from datetime import datetime
import glob


path = "*.csv"
for fname in glob.glob(path):


    output_str = ""
    
    with open(fname) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')

        for row in csv_reader:

            super_counter = 0
            firstone = True

            output_str += "["
            for qw in row:

                if super_counter == 0 and firstone:
                    output_str += "{"
                    output_str += "\"Monitor_id\": "
                    output_str += str(qw) + ","
                    firstone = False

                elif super_counter == 0 and not firstone:
                    output_str += ",{"
                    output_str += "\"Monitor_id\": "
                    output_str += str(qw) + ","

                elif super_counter == 1:
                    output_str += "\"FFT_axis\": "
                    output_str += "\"" + str(qw) + "\","

                elif super_counter == 2:
                    output_str += "\"Timestamp\": "

                    #2021-03-17T04:16:50Z
                    time_format = "%Y-%m-%dT%H:%M:%S.%fZ" if '.' in str(qw) else "%Y-%m-%dT%H:%M:%SZ"
                    utc_time = datetime.strptime(str(qw), time_format)
                    epoch_time = (utc_time - datetime(1970, 1, 1)).total_seconds()

                    output_str += "\"" + str(epoch_time) + "\","

                elif super_counter == 3:
                    output_str += "\"FFT_Freqency_array\": "

                    output_str += "["
                    output_str += str(qw)
                    output_str += "],"

                elif super_counter == 4:
                    output_str += "\"FFT_amplitude_array\": "

                    output_str += "["
                    output_str += str(qw)
                    output_str += "]"
                    output_str += "}"

                else:
                    pass

                super_counter += 1

                if super_counter == 5:
                    super_counter = 0

            output_str += "]"
    
    f = open(fname + ".json", "w")
    f.write(output_str)
    f.close()
        
