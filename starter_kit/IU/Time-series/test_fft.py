import csv
import json
from time import time
import numpy as np

dataY = []

with open('1_rms_125_1_ok.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    
    counter = 0
    for row in csv_reader:
        if counter > 0:
            dataY.append(row[4])
        else:
            counter += 1

superr = np.fft.fft(dataY)

amp = np.abs(superr)

print(amp)