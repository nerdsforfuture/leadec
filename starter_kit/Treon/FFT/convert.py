import csv
import numpy as np
from datetime import datetime
import glob

path = "*.csv"
for fname in glob.glob(path):


    output_str = ""
    
    with open(fname) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')

        for row in csv_reader:

            super_counter = 0
            firstone = True


            output_str += "["
            firstchecker = True
            for qw in row:

                if super_counter == 0 and firstchecker:
                    output_str += "{\"Timestamp\": " + str(qw) + ","
                    firstchecker = False
                elif super_counter == 0 and not firstchecker:
                    output_str += ",{\"Timestamp\": " + str(qw) + ","
                else:
                    pass

                if super_counter == 1:
                    output_str += "\"FFT_axis\": \"" + str((qw)) + "\","
                
                if super_counter == 2:
                    if len(qw.split(",")) > 1:
                        aaa = [float(ss) for ss in qw.split(",")]

                        output_str += "\"FFT_array\": " + str(aaa) + "}"
                    else:
                        output_str += "\"FFT_array\": [" + str(qw.split(",")[0]) + "]}"

                super_counter += 1

                if super_counter == 3:
                    super_counter = 0

            output_str += "]"

    f = open(fname + ".json", "w")
    f.write(output_str)
    f.close()
        


