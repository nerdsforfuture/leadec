import tensorflow as tf
import tensorflow.keras as keras
import sklearn
from data.IU.FFT.preprocessing_iu_fft import *


class leadec_model(keras.Model):
    def __init__(self, n_compoments):
        super(leadec_model, self).__init__()
        self.model = self.direct_model(n_compoments)

    def direct_model(self, n_components):
        model = keras.Sequential()
        model.add(keras.layers.Dense(n_components))
        model.add(keras.layers.Dense(2 * n_components, activation='relu'))
        model.add(keras.layers.Dense(4 * n_components, activation='relu'))
        model.add(keras.layers.Dense(1))
        return model

    def call(self, inputs, training=None, mask=None):
        score = self.model(inputs)
        return score

    def train_step(self, data):
        # Unpack the data. Its structure depends on your model and
        # on what you pass to `fit()`.
        x, y = data

        with tf.GradientTape() as tape:
            y_pred = self(x, training=True)  # Forward pass
            # Compute the loss value
            # (the loss function is configured in `compile()`)
            loss = self.compiled_loss(y, y_pred)

        # Compute gradients
        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)
        # Update weights
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))
        # Update metrics (includes the metric that tracks the loss)
        self.compiled_metrics.update_state(y, y_pred)
        # Return a dict mapping metric names to current value
        return {'loss': loss,
                'predicted_score': tf.reduce_mean(y_pred)}

    def test_step(self, data):
        # Unpack the data
        x, y = data
        # Compute predictions
        y_pred = self(x, training=False)

        # Updates the metrics tracking the loss
        # Update the metrics.
        # Return a dict mapping metric names to current value.
        # Note that it will include the loss (tracked in self.metrics).
        # return {m.name: m.result() for m in self.metrics}
        return {'predicted_score': tf.reduce_mean(y_pred)}


n_components = 10

#fft_y_125_1 (630, 10)
x_train_1 = preprocessing_iu_fft('data/IU/FFT/1_FFT_Y_125_1_ok.csv.json', n_components, 0, 1, False)
#fft_y_125_2 (627, 10)
x_train_2 = preprocessing_iu_fft('data/IU/FFT/1_FFT_Y_125_2_ok.csv.json', n_components, 0, 1, False)


x_test_2 = preprocessing_iu_fft('data/IU/FFT/1_FFT_Y_125_2_test.csv.json', n_components, 0, 1, True)

y_train_1 = tf.zeros(shape=(630, 1))
y_train_2 = tf.zeros(shape=(627, 1))

model = leadec_model(n_compoments=n_components)

model.compile(optimizer="adam", loss=keras.losses.MeanSquaredError())
model.fit(x_train_2, y_train_2, batch_size=128, epochs=30)
model.evaluate(x_test_1, None, batch_size=128, return_dict=True)
