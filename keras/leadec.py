import tensorflow as tf
import tensorflow.keras as keras
from tensorflow.keras.Layers import Dense


def setup_model(modeltype='autoencoder', fft_length, sensor_type):
    if modeltype is 'autoencoder':
        direct_model = direct_model = leadec_model(fft_length=fft_length, sensor_type=sensor_type)
    elif modeltype is other:
        direct_model = keras.Model() # TODO implement the other algorithm for anomaly detection
        pass
    direct_model.compile(optimizer='adam', loss=keras.losses.mean_squared_error)


def model(self, n_fft):
    sequential = keras.Sequential()
    sequential.add(Dense(n_fft))
    return sequential

# keras model gerüst TODO: implement neural network
class leadec_model(keras.Model):
    def __init__(self, fft_length, sensor_type, name='LEADEC', **kwargs):
        super(leadec_model, self).__init__(name=name)
        self.n_fft = fft_length
        self.sensor = sensor_type
        self.model = self.model()

    def call(self, inputs, training=None, mask=None):
        out = self.model(inputs)
        return out

    def train_step(self, data):
        # Unpack the data. Its structure depends on your model and
        # on what you pass to `fit()`.
        x, y = data

        with tf.GradientTape() as tape:
            y_pred = self(x, training=True)  # Forward pass
            # Compute the loss value
            # (the loss function is configured in `compile()`)
            loss = self.compiled_loss(y, y_pred, regularization_losses=self.losses)

        # Compute gradients
        trainable_vars = self.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)
        # Update weights
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))
        # Update metrics (includes the metric that tracks the loss)
        self.compiled_metrics.update_state(y, y_pred)
        # Return a dict mapping metric names to current value
        return {m.name: m.result() for m in self.metrics}
