import os
import logging
import sys
import pandas as pd
import numpy as np
import pickle
import argparse
import re
import tempfile
import glob
import distutils
import datetime
import subprocess
import time
import threading, queue
import shutil
import json
import leadec
import argparse
import sklearn


# overview of parameters
parser =argparse.ArgumentParser(description='Leadec-Task')

parser.add_argument('--optimizer', default='adam',
                    help='keras compile optimizer parameter value')
parser.add_argument('--loss', default=keras.losses.mean_squared_error,
                    help='keras compile loss parameter value')
parser.add_argument('--mode', default='preprocess',
                    help='mode: preprocess, train, test')
parser.add_argument('--modeltype', default='autoencoder',
                    help='modeltype to do anomaly detection')

def dataset_generator():
    pass

# Function to load Dataset - Sensortypes: IU or TREON
def load_dataset(sensortype='IU'):
    pass


def preprocess_pipeleine():
    pass


def setup():
    pass


def train_model(modeltype, optimizer, loss_function):
    pass


def evaluate_model():
    pass


if __name__=='__main__':
    args = parser.parse_args()
    # TODO: CUDA schnittstelle
    if args.mode is 'train':
        train_model(args.modeltype, args.optimizer, args.loss)
        pass
    elif args.mode is 'preprocess':
        pass
    elif args.mode is 'eval':
        pass
    elif args.mode is 'plot':
        pass
    else:
        print('Wrong moder gurl, options: train, preprocess, eval, plot')



