#NerdsForFuture

## Datenaufbereitung
Alle aufbereiteten Daten sind im Order data zu finden. Gleich Dateistruktur wie in starter_kit.
Ich habe die FFTs für IU und Treon als .json File abgelegt und in die entsprechenden Ordner jeweils ein Beispielscript gelegt, wie die Daten in Pandas geladen werden können. 
Die Scripte mit denen ich die Daten aufbereitet habe, liegen im "starter_kit" Ordner in den entsprechenden FFT-Ordnern. 
Das ganze funktioniert halbwegs automatisiert. Es werden alle CSV Dateien im Ordner bearbeitet. Da müssen wir mal schauen wie wir das noch zusammen basteln das diese Daten am Ende vom "großen" Script automatisch verarbietet und in die entsprechenden Ordner verschoben wird. 
Für die Zeitreihen war es nicht nötig die Daten zu verändern (Ich habe allerdings die Excel Dateien in CSV umgewandelt). Alle CSV Dateien zu Zeitreihen liegen auf im entsprechenden Unterordner von "data". Auch hier befindet sich ein Beispielscript zum laden in Pandas bereit. 

Ich habe die Timestamps der FFT daten umgewandelt in Epoch Format. In den CSV Dateien der Zeitreihen aber nicht. Pandas erkennt es aber richtig und generiert daraus ein eigenes Zeitfomat. 

Soweit, schaut am besten nochmal rüber ob das Umwandeln auch wirklich richtig geklappt hat.

Bis morgen ;)
