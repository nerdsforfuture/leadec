import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA 
from sklearn.preprocessing import MinMaxScaler
from sklearn.pipeline import make_pipeline
import numpy as np

def preprocessing_iu_fft(file_path_json, number_pcas, x_min, x_max, plot=False):
    patients_df = pd.read_json(file_path_json)

    x_read = np.array(patients_df['FFT_amplitude_array'].to_list(), dtype='object')

    # check if frequency grid is equal for all datapoints
    num_amps = list(map(lambda data: len(data), x_read))
    max_num_amps = np.max(num_amps)
    valid = num_amps == max_num_amps
    x = x_read[valid].tolist()

    pca = PCA(n_components=number_pcas)
    scaler = MinMaxScaler((x_min,x_max))

    # PCA Transform 
    x_new = pca.fit_transform(x)

    # 0 - 1 Sclaing
    scaler.fit(x_new)
    x_new = scaler.transform(x_new)

    if plot:
        plot_pca_variance(pca)

    return x_new

def plot_pca_variance(pca):
    features = range(pca.n_components_)

    plt.figure(figsize=(15, 5))
    plt.bar(features, pca.explained_variance_)
    plt.xlabel('PCA feature')
    plt.ylabel('Variance')
    plt.xticks(features)
    plt.title("Importance of the Principal Components based on inertia")
    plt.show()

# ready to haemmer in DeepNet